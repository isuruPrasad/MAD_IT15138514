//
//  TemperatureViewController.swift
//  IT15138514
//
//  Created by Madhuwantha, Isuru on 8/16/18.
//  Copyright © 2018 Madhuwantha, Isuru. All rights reserved.
//

import UIKit

class TemperatureViewController: UIViewController {

    @IBOutlet weak var txbCelcius: UITextField!
    @IBOutlet weak var txbFarenheit: UITextField!
    @IBOutlet weak var txbKelvin: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func txbChanged(_ sender: Any) {
        let text = (sender as! UITextField).text
        
        if text == nil || text!.isEmpty {
            txbCelcius.text = ""
            txbKelvin.text = ""
            txbFarenheit.text = ""
            return
        }
        
        let value = Double.init(text!)
        if value == nil {
            return
        }
        
        if ((sender as! UITextField).isEditing) {
            if (sender as! UITextField) == self.txbCelcius {
                setKelvinValue(value: value! + 273.15)
                setFarenheitValue(value: value! * 9/5 + 32)
            } else if (sender as! UITextField) == self.txbFarenheit {
                setCelciusValue(value: (value! - 32) * 5/9)
                setKelvinValue(value: (value! + 459.67) * 5/9)
            } else if (sender as! UITextField) == self.txbKelvin {
                setFarenheitValue(value: value! * 9/5 - 459.67 )
                setCelciusValue(value: value! - 273.15)
            }
        }
    }
    
    func setCelciusValue(value: Double) {
        txbCelcius.text = "\(value)"
    }
    
    func setFarenheitValue(value: Double) {
        txbFarenheit.text = "\(value)"
    }
    
    func setKelvinValue(value: Double) {
        txbKelvin.text = "\(value)"
    }
    
}
