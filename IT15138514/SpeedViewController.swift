//
//  SpeedViewController.swift
//  IT15138514
//
//  Created by Madhuwantha, Isuru on 8/16/18.
//  Copyright © 2018 Madhuwantha, Isuru. All rights reserved.
//

import UIKit

class SpeedViewController: UIViewController {
    @IBOutlet weak var txbmph: UITextField!
    @IBOutlet weak var txbFpm: UITextField!
    @IBOutlet weak var txbKph: UITextField!
    @IBOutlet weak var txbMilesPerHour: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func txbChanged(_ sender: Any) {
        let text = (sender as! UITextField).text
        
        if text == nil || text!.isEmpty {
            txbmph.text = ""
            txbKph.text = ""
            txbMilesPerHour.text = ""
            txbFpm.text = ""
            return
        }
        
        let value = Double.init(text!)
        if value == nil {
            return
        }
        
        if ((sender as! UITextField).isEditing) {
            if (sender as! UITextField) == self.txbmph {

            } else if (sender as! UITextField) == self.txbFpm {

            } else if (sender as! UITextField) == self.txbKph {
   
            } else if (sender as! UITextField) == self.txbMilesPerHour {
                
            }
        }
    }
    
    func setMetrePerSecValue(value: Double) {
        txbmph.text = "\(value)"
    }
    
    func setFeetPerMinuteValue(value: Double) {
        txbFpm.text = "\(value)"
    }
    
    func setkiloMetrePerHourValue(value: Double) {
        txbKph.text = "\(value)"
    }
    
    func setMilesPerHourValue(value: Double) {
        txbMilesPerHour.text = "\(value)"
    }
    
}
    

